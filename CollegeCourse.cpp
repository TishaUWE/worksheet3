#include <iostream>

using namespace std;

// Instructor class
class Instructor {
private:
    string firstName;
    string lastName;
    int officeNumber;
public:
    Instructor(string fName, string lName, int office) : firstName(fName), lastName(lName), officeNumber(office) {}
    //displaying data
    void display() {
        cout << "Instructor: " << firstName << " " << lastName << endl;
        cout << "Office: " << officeNumber << endl;
    }
};

// Classroom class
class Classroom {
private:
    string building;
    int roomNumber;
public:
    Classroom(string build, int room) : building(build), roomNumber(room) {}
   //displaying data
    void display() {
        cout << "Classroom: " << building << " " << roomNumber << endl;
    }
};

// CollegeCourse class
class CollegeCourse {
private:
    Instructor instruct;
    Classroom classm;
    int credits;
public:
    CollegeCourse(string fName, string lName, int office, string build, int room, int credit) : instruct(fName, lName, office), classm(build, room), credits(credit) {}
    //displaying data
    void display() {
        instruct.display();
        classm.display();
        cout << "Credits: " << credits << endl;
    }
};

int main() {
    CollegeCourse course1("Subrat", "Rayamajhi", 111, "Math Building", 112, 4);
    CollegeCourse course2("Sadisha", "Shrestha", 666 , "IT Building",222, 9);
    course1.display();
    cout << endl;
    course2.display();
    return 0;
}
