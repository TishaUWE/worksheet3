#include<iostream>

using namespace std;

class Salesperson; //forward declaration

class Sale{
    private:
    string date;
    float amount;
    int person_id;

    void friend display(Sale, Salesperson);

    public:
    Sale(string date, float amount, int id){
        this->date = date;
        this->amount = amount;
        this->person_id = id;   

    }
    
      
};

class Salesperson
{
    private:
    int sale_id;
    string lname;

    void friend display(Sale, Salesperson);//friend declaration in public mode

    public:
    Salesperson(int id, string lname){
        this->sale_id = id;
        this->lname = lname;
    }

     
};

void display(Sale sale, Salesperson person) // defination of friend function
{
    cout <<"Date of sale: "<< sale.date << endl;   // accessig private member of classes
    cout<<"Amount of sale: "<< sale.amount << endl;
    cout<<"SalesPerson ID: "<< sale.person_id <<endl;
    cout<<"SalesPerson Name: "<<person.lname<<endl;
}

int main(){
 

   Sale sale("2002/11/10",1400,1);
   Salesperson person(12,"shrestha");
    display(sale, person);
   


}