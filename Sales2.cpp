#include <iostream>

using namespace std;

class Salesperson; // forward declaration

class Sale
{
private:
    string date;
    float amount;
    int person_id;

    void friend display(Sale, Salesperson);

public:
    Sale(string date, float amount, int id)
    {
        this->date = date;
        this->amount = amount;
        this->person_id = id;
    }

    int get_id() //constructor declartion
    
    {
        return person_id;
    }
};

class Salesperson
{
private:
    int sale_id;
    string lname;

    void friend display(Sale, Salesperson); // friend declaration in public mode

public:
    Salesperson(int id, string lname) //constructor declartion
    {
        this->sale_id = id;
        this->lname = lname;
    }

    int get_id() //defining function that returns id
    {
        return sale_id;
    }
};

void display(Sale sale, Salesperson person) // defination of friend function
{
    cout << "Date of sale: " << sale.date << endl; // accessing private member of classes
    cout << "Amount of sale: " << sale.amount << endl;
    cout << "SalesPerson ID: " << sale.person_id << endl;
    cout << "SalesPerson Name: " << person.lname << endl;
}

int main()
{
    string date;
    float amount;
    int person_id;
   
   

    // Sale sale("sunday",1400,1);
    // Salesperson person(12,"shrestha");
    // display(sale, person);
 
    Salesperson person[5] = { //five salesperson object
        Salesperson(1, "Ram"),
        Salesperson(2, "Shyam"),
        Salesperson(3, "Hari"),
        Salesperson(4, "Krishna"),
        Salesperson(5, "Murali")};

    while (true)
    {
        //asking user for sale data
        cout << "Enter the date of sale (or 'q' to quit): ";
        cin >> date;
        if (date == "q")
            break;
        cout << "Enter the amount of sale: " << endl;
        cin >> amount;
        cout << "Enter your id: " << endl;
        cin >> person_id;

        bool valid_id = false;

        for (int i = 0; i < 5; i++) //checking for valid id
        {
            if (person[i].get_id() == person_id)
            {
                valid_id = true;
                break;
            }
        }

        if (valid_id) //displaying data
        {
            Sale sale(date, amount, person_id);
            display(sale, person[person_id]);
        }
        else
        {
            cout << "Salesperson ID needs to be registered" << endl;
        }
    }
    return 0;
};
